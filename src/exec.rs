use crate::progtype::ProgType;
use async_std::future;
use async_std::stream::StreamExt;
use nix::sys::signal::{kill, Signal};
use nix::unistd::{execvp, fork, getpid, getppid, ForkResult};
use std::error::Error;
use std::ffi::{CStr, CString};
use std::time::Duration;
use swayipc::reply::{Event, WindowChange};
use swayipc::{Connection, EventType};

pub fn exec_hup(executable: &str) -> Box<dyn Error> {
    let executable = CString::new(executable.to_string()).unwrap();
    let executable: &CStr = executable.as_c_str();

    unsafe {
        libc::signal(libc::SIGHUP, libc::SIG_IGN);
    }
    execvp(executable, &[executable]).expect_err("Execvp should not return");
    unreachable!();
}

pub fn exec_term(executable: &str) -> Result<(), Box<dyn Error>> {
    match fork() {
        // Exec_hup only returns on error
        Ok(ForkResult::Parent { .. }) => {
            exec_hup(executable);
            unreachable!();
        }
        Ok(ForkResult::Child) => Ok(()),
        Err(err) => Err(Box::new(err)),
    }
}

pub async fn exec_watch(executable: &str) -> Result<ProgType, Box<dyn Error>> {
    let term_pid = getppid();
    let _process_pid = getpid();
    match fork() {
        // Exec_hup only returns on error
        Ok(ForkResult::Parent { .. }) => Err(exec_hup(executable)),
        Ok(ForkResult::Child) => future::timeout(Duration::from_secs(5), async {
            // establish connection.
            let mut events = Connection::new()
                .await
                .unwrap()
                .subscribe(&[EventType::Window])
                .await
                .unwrap();
            while let Some(event) = events.next().await {
                if let Ok(event) = event {
                    match event {
                        Event::Window(w) => {
                            // Window created. Kill parents parent
                            // Child -> parent -> terminal emulator

                            if w.change == WindowChange::New {
                                if let Some(_con_pid) = w.container.pid {
                                    // This may not recognize some forking windowed programs correctly
                                    // Could be done by looking at the process tree
                                    // Not checking pid ends up killing wrong processes if those are creating windows
                                    if true {
                                        // process_pid.as_raw() == con_pid {
                                        kill(term_pid, Signal::SIGTERM)
                                            .expect("Cannot kill TERM emulator");
                                        return Ok(ProgType::Windowed);
                                    }
                                }
                            }
                        }
                        // Subsribed to only window events
                        _ => unreachable!(),
                    }
                }
            }
            // The iterator should never end.
            unreachable!();
        })
        .await
        .unwrap_or(Ok(ProgType::Terminal)),
        Err(err) => Err(Box::new(err)),
    }
}

pub async fn exec_windowed(executable: &str) -> Result<(), Box<dyn Error>> {
    let mut con = Connection::new().await?;
    con.run_command(format!("exec {}", executable)).await?;
    Ok(())
}
