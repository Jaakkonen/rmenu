use crate::ProgType;
use skim::{AnsiString, SkimItem};
use std::borrow::Cow;

#[derive(Savefile, Debug, Clone)]
pub struct MenuItem {
    //name: String,
    pub executable: String,
    pub inode: u64,
    pub progtype: Option<ProgType>,
    pub name: Option<String>,
}
impl MenuItem {
    fn name(&self) -> &str {
        match &self.name {
            Some(n) => &n,
            None => &self.executable,
        }
    }
}

impl SkimItem for MenuItem {
    fn display(&self) -> Cow<AnsiString> {
        Cow::Owned(self.name().into())
    }
    fn text(&self) -> Cow<str> {
        Cow::Borrowed(self.name())
    }
}
