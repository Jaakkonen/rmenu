#[derive(Savefile, Debug, Clone)]
pub enum ProgType {
    Windowed,
    Terminal,
}
