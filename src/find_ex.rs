use crate::MenuItem;
use std::collections::hash_set::HashSet;
use std::fs::read_dir;
use std::os::unix::fs::MetadataExt;

pub fn get_executables() -> Box<dyn Iterator<Item = MenuItem>> {
    Box::new(
        std::env::var("PATH")
            .unwrap()
            .split(':')
            .map(|x| match std::fs::read_link(x) {
                Ok(p) => p.to_string_lossy().to_string(),
                Err(_) => x.to_string(),
            })
            .collect::<HashSet<String>>()
            .into_iter()
            .filter_map(|path| read_dir(path).ok())
            .flatten()
            .filter_map(|r| r.ok())
            .map(|de| (de.metadata().ok(), de))
            .filter(|de| de.0.is_some())
            .map(|de| (de.1, de.0.unwrap()))
            .filter(|de| (de.1.mode() & 0o111) != 0)
            .map(|f| MenuItem {
                executable: f.0.file_name().to_string_lossy().into_owned(),
                inode: f.1.ino(),
                progtype: None,
                name: None, // Used only if executable != name
            }),
    )
}
