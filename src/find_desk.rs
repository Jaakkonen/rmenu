use crate::{progtype::ProgType, MenuItem};
use std::fs::read_dir;
extern crate ini;
use ini::Ini;
use std::os::unix::fs::DirEntryExt;
extern crate regex;
use regex::Regex;

struct IniInodeEntry {
    ini: Ini,
    inode: u64,
}

pub fn get_desktop_files() -> Box<dyn Iterator<Item = MenuItem>> {
    // WARN: Rouva Siiranen soittaa.
    lazy_static! {
        static ref RE: Regex = Regex::new(r"%.").unwrap();
    }
    let home = std::env::var("HOME").expect("No home");
    let local_path = format!("{}/.local/share/applications", home);
    let app_path = vec![
        "/usr/share/applications".to_string(),
        "/usr/local/share/applications".to_string(),
        local_path,
    ];
    let apps = app_path
        .into_iter()
        .filter_map(|d| read_dir(d).ok())
        .flatten()
        .filter_map(|x| x.ok()) // File is readable
        .filter_map(|x| match Ini::load_from_file(x.path()) {
            Ok(i) => Some(IniInodeEntry {
                ini: i,
                inode: x.ino(),
            }),
            _ => None,
        })
        .filter_map(|x: IniInodeEntry| {
            let sec = x.ini.section(Some("Desktop Entry"))?;
            let name = sec.get("Name")?.to_string();
            // TODO: Add support for the magic field thingys
            let executable = sec.get("Exec")?;

            let executable = RE.replace_all(&executable, " ");
            // TODO: Check that TryExec binary is found.
            Some(MenuItem {
                name: Some(name.into()),
                executable: executable.into(),
                inode: x.inode,
                progtype: Some(ProgType::Windowed),
            })
        });
    return Box::new(apps);
}
