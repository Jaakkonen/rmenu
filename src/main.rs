#![feature(test)]

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;

mod exec;
mod progtype;
mod util;
use progtype::ProgType;
use util::OptionExitGraceful;

mod find_ex;
use find_ex::get_executables;

mod find_desk;
use find_desk::get_desktop_files;

mod menuitem;
use menuitem::MenuItem;

extern crate savefile;
use savefile::prelude::*;

#[macro_use]
extern crate savefile_derive;
extern crate async_std;
extern crate crossbeam;
extern crate libc;
extern crate nix;
extern crate skim;
extern crate swayipc;
use crossbeam::unbounded;
use skim::prelude::*;
use std::collections::BTreeSet;
use std::error::Error;
use std::sync::Mutex;
use std::thread;

#[derive(Savefile)]
struct SavedItems {
    recents: Vec<MenuItem>,
}

fn get_cache_path() -> String {
    let cache = std::env::var("HOME").unwrap_or("/tmp".to_string());
    format!("{}/.cache/rmenu_cache.bin", cache)
}

fn load_recent() -> SavedItems {
    let path = get_cache_path();
    load_file(path.as_str(), 0).unwrap_or(SavedItems { recents: vec![] })
}

fn save_recent(s: &SavedItems) -> Result<(), SavefileError> {
    let path = get_cache_path();
    save_file(path.as_str(), 0, s)
}

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    //let mut executables = HashSet::new();
    let used_inodes = Arc::new(Mutex::new(BTreeSet::new()));
    //let paths = get_path_map()?;
    let (s, r): (SkimItemSender, SkimItemReceiver) = unbounded();
    //let mut recents = Arc::new(load_recent());
    let t_used_inodes = used_inodes.clone();
    thread::spawn(move || {
        let mut t_used_inodes = t_used_inodes.lock().unwrap();
        for item in load_recent().recents {
            t_used_inodes.insert(item.inode);
            s.send(Arc::new(item)).unwrap();
        }
        for d in get_desktop_files() {
            if !t_used_inodes.contains(&d.inode) {
                s.send(Arc::new(d)).unwrap();
            }
        }
        for d in get_executables() {
            if !t_used_inodes.contains(&d.inode) {
                s.send(Arc::new(d)).unwrap();
            }
        }
    });
    let opts = SkimOptionsBuilder::default().build()?;
    let mut skim = Skim::run_with(&opts, Some(r)).unwrap_or_exit();
    // Read the examples...
    // Found from lib.rs example documentation at
    // https://github.com/lotabout/skim/blob/master/src/lib.rs

    let selected_item = skim.selected_items.first_mut().expect("No prog selected");
    let item = Arc::get_mut(selected_item)
        .expect("Other thread using item")
        .as_any_mut()
        .downcast_mut::<MenuItem>()
        .expect("Should be MenuItem");

    // This will not return from the main process (execvp)
    // In the child process this should return a ProgType
    match item.progtype.clone() {
        Some(ProgType::Windowed) => exec::exec_windowed(&item.executable).await?,
        Some(ProgType::Terminal) => exec::exec_term(&item.executable)?,
        None => item.progtype = Some(exec::exec_watch(&item.executable).await?),
    }
    debug!("Progtype: {:?}", item.progtype);
    let mut reccents = load_recent();
    // Find old entry with same inode and remove it
    reccents
        .recents
        .iter()
        .position(|i| i.inode == item.inode)
        .map(|i| reccents.recents.remove(i));
    // Add new entry
    //reccents.recents.insert(0, item);
    reccents.recents.insert(0, item.clone());
    save_recent(&reccents).unwrap();
    //unreachable!();
    Ok(())
}

extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn executables(b: &mut Bencher) {
        b.iter(|| get_executables().into_iter().collect::<Vec<_>>());
    }
}
