pub use swayipc::Fallible;
pub trait OptionExitGraceful<T> {
    fn unwrap_or_exit(self) -> T;
}
impl<T> OptionExitGraceful<T> for Option<T> {
    fn unwrap_or_exit(self) -> T {
        self.unwrap_or_else(|| {
            std::process::exit(0);
        })
    }
}

impl<T, K> OptionExitGraceful<T> for Result<T, K> {
    fn unwrap_or_exit(self) -> T {
        self.unwrap_or_else(|_| {
            std::process::exit(0);
        })
    }
}
