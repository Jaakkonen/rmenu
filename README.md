# rmenu - Launch terminal and windowed apps the same way.

Wanna launch firefox, qtcreator or python shell the same way? Use rmenu.
It recognizes if your application type and launches it accordingly utilizing the same term emulator the fuzzy finder uses for interactive shells.

## Usage

Install:
```
git clone https://gitlab.com/jaakkonen/rmenu
cd rmenu
cargo install --path .
``` 
or 
```
cargo install --git https://gitlab.com/jaakkonen/rmenu
```

Sway config:
```
# Run rmenu in alacritty/your term of choise
set $menu alacritty --class=rmenu -e rmenu
# Make rmenu window floating
for_window [app_id="rmenu"] floating enable
# Menu launch binding
bindsym $mod+d exec $menu
```

## Dependencies

- Latest stable Rust
- Sway window manager
  - Recognizing creation of new window depends on sways IPC protocol.

## Inspired by

fzf 
- It's blazing fast fuzzy-search tool. I wanted to use it in my launcher

wofi - It looks nice when launching my apps
However,
- Super slow search
- Cannot launch terminal apps

dmenu
- I like the style of launching windowed programs from my PATH
- dmenu_path is nice, but doesn't show my new programs. I want those to always show up


## TODO

See if rusts threads/processes could be used in place of fork()

Check if window was created by launched process or a child process of it prior deciding its a windowed application

Add support for `.desktop` applications

